<?php
error_reporting(E_ALL);
ini_set('display_errors', '1');

$regerror = $abserror = array('code' => 0, 'text' => '', 'data' => array());
?>
<!DOCTYPE html>
<html>
    <head>
        <title>ICBA 2018</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="icon" href="favicon.ico" type="image/x-icon"/>
        <link rel="shortcut icon" href="favicon.ico" type="image/x-icon"/>
        <link rel="stylesheet" href="css/lib/w3.css">
        <link rel="stylesheet" href="css/w3.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="css/scrolling.css">
         <link rel="stylesheet" href="css/main.css?ver=2">
         <link class="colorScheme" rel="stylesheet" href="css/colorTheme_2.css">

        
        <script type="text/javascript" src="js/jquery-latest.min.js" ></script>
        <script src="js/jquery.matchHeight.js" type="text/javascript"></script>
    </head>
    <body onload="combine()">

        <?php echo $regerror['text'] . $abserror['text']; ?>
        <?php include 'nav.php' ?>              
        

        <a name="home"></a>
        <div id="home" class="bgimg-1 w3-center">
            <div class="w3-third w3-black w3-display-middle">
            </div>
        </div>

        <!-- About -->
        <a name="about"></a>
        <div class="w3-content w3-container w3-padding-64" id="about">
            <h3 class="w3-left-align">Dear Colleagues,</h3>
              <p class="w3-justify">
                  We are pleased to announce that the 5th International Conference on Behavioral Addictions (ICBA) is due to take place from the 23rd-25th of April, 2018 in Cologne, Germany. The conference will be organized by Astrid Müller (Hannover Medical School, Germany), Matthias Brand (University of Duisburg-Essen, Germany), and the president of the newly founded International Society for the Study of Behavioral Addictions (ISSBA) Zsolt Demetrovics (Eötvös Loránd University, Hungary).
            </p>
            <p class="w3-justify">This conference in Cologne will provide a unique opportunity to share scientific knowledge on non-substance related addictions ranging from genetic and neurobiological research to psychological and clinical approaches in epidemiological, sociological and anthropological fields. The ICBA is an excellent chance to meet experts in the field of behavioral addictions and to facilitate the development of new international networks and collaborations. 
            We look forward to welcoming everyone who is interested in behavioral addictions and related disorders, whatever their perspective. Join us in Cologne.
            </p>
            <p>Yours sincerely,</p>
            <div id="conferenceChairs" class="w3-row">
                 <div class="w3-col m4 w3-section w3-center">
                    <img src="images/Brand_Matthias.jpg"/>
                    <div class="w3-padding-16">
                        Matthias Brand<br />Conference Chair<br /> 
                    </div>
                    <div class="w3-border-top w3-center" style="min-height: 120px">
                     <img style="width:60%;margin-top: 23px" src="images/ude.png"/>
                     </div>
                </div>
                <div class="w3-col m4 w3-section w3-center">
                   <img src="images/Mueller_Astrid_neu_sw.jpg" />
                    <div class="w3-padding-16"> 
                        Astrid Müller <br />Conference Chair<br/>
                    </div>
                    <div class="w3-border-top w3-center" style="min-height:120px;">
                    <img src="images/MHH_Logo_engl_grau.jpg" style="width:80%;margin-top:38px"/>
                    </div>
                </div>
               
                <div class="w3-col m4 w3-section w3-center">
                    <img src="images/Demetrovics_Zsolt_sw.jpg">
                    <div class="w3-padding-16">       
                        Zsolt Demetrovics<br />President of the ISSBA<br />
                    </div>
                    <div class="w3-border-top w3-center" style="min-height: 120px">
                    <img src="images/issba_logo.png" style="width:42%;margin-top:33px"/>
                    </div>
                </div>
            </div>
        </div>
        <!-- Keynote speakers -->
        <a name="keynotespeakers"></a>
        <div id="keynotespeakers">
        <div class="bgimg-4 w3-display-container">
            <div class="w3-display-middle">
                <div class="w3-hide-small w3-hide-medium w3-center w3-padding-large w3-black w3-xxlarge w3-wide w3-animate-opacity">Keynote speakers</div>
                <div class="w3-hide-small w3-hide-large w3-center w3-padding-medium w3-black w3-xlarge w3-wide w3-animate-opacity">Keynote speakers</div>
                <div class="w3-hide-medium w3-hide-large w3-center w3-padding-small w3-black w3-large w3-wide w3-animate-opacity">Keynote speakers</div>
            </div>
        </div>
        <div class="keynoteSpeaker w3-content w3-padding-32">
            <!--[if IE]>
            <div class="w3-container">
                <div class="w3-col s6 m4">
                    <div class="w3-col s4">
                         <img width="60%" src="images/Billieux_Joel_sw.jpg">
                     </div>
                      <div class="w3-col s8">
                         <strong>Joël Billieux</strong><br />
                     University of Luxembourg<br />(Luxembourg)<br />
                     </div>
                </div>
                <div class="w3-col s6 m4">
                    <div class="w3-col s4">
                        <img  width="60%" src="images/Clark_Luke_sw.jpg"   class="">
                    </div>
                    <div class="w3-col s8">
                        <strong>Luke Clark</strong><br />
                        University of British Columbia<br/>(Canada)<br />
                    </div>
                </div>
                <div class="w3-col s6 m4">
                    <div class="w3-col s4">
                        <img  width="60%" src="images/Demetrovics_Zsolt_sw.jpg"   class="">
                     </div>
                     <div class="w3-four-third" >
                      <strong>Zsolt Demetrovics</strong><br />
                         Eötvös Loránd University<br />(Hungary)<br />
                     </div>
                </div>
                <div class="w3-col s6 m4">
                    <div class="w3-col s4">
                        <img width ="60%" src="images/Dong_Guangheng_sw.jpg"   class="">
                     </div>
                     <div class="w3-four-third" >
                         <strong>Guangheng Dong</strong><br />
                         Zhejiang Normal University Jinhua<br />(China)
                    </div>
                </div>
                <div class="w3-col s6 m4">
                    <div class="w3-col s4">
                <img  width="60%" src="images/FernandezAranda_Fernanddo_sw.jpg"  >
                     </div>
                     <div class="w3-four-third" >
                       <strong>Fernando Fernandez-Aranda</strong><br />
                         University Hospital Bellvitge (Spain)
                     </div>
                </div>
                <div class="w3-col s6 m4">
                    <div class="w3-col s4">
                        <img width="60%" src="images/Gearhardt_Ashley_sw.jpg"  >
                     </div>
                     <div class="w3-four-third" >
                       <strong>Ashley Gearhardt</strong><br />
                        University of Michigan <br />(United States)<br />
                    </div>
                </div>
                <div class="w3-col s6 m4">
                    <div class="w3-col s4">
                        <img width="60%" src="images/King_Daniel_sw.jpg"   class="">
                      </div>
                     <div class="w3-four-third" >
                   <strong>Daniel King</strong><br />
                     University of Adelaide<br />(Australia)<br />
                    </div>
                </div>

                <div class="w3-col s6 m4">
                    <div class="w3-col s4">
                        <img width="60%" src="images/kyrios_michael_sw.jpg"   class="">
                      </div>
                     <div class="w3-four-third" >
                     <strong>Michael Kyrios</strong><br />
                     Australian National University<br />(Australia)<br />
                    </div>
                </div>
                <div class="w3-col s6 m4">
                    <div class="w3-col s4">
                        <img width="60%" src="images/Kuss_Daria_sw.jpg"   class="">
                    </div>
                    <div class="w3-four-third" >
                        <strong>Daria Kuss</strong><br />
                        Nottingham Trent University<br />(United Kingdom)
                    </div>
                </div>
                <div class="w3-col s6 m4">
                    <div class="w3-col s4">
                        <img width="60%" src="images/Rumpf_Hans-Juergen_sw.jpg"   class="">
                    </div>
                    <div class="w3-four-third" >
                        <strong>Hans-Jürgen Rumpf</strong><br />
                        Medical University of Lübeck<br/>(Germany)
                    </div>
                </div>
                <div class="w3-col s6 m4">
                    <div class="w3-col s4">
                        <img width="60%" src="images/Spada_Marcantonio_M_sw.jpg"   class="">
                      </div>
                     <div class="w3-four-third" >
                     <strong>Marcantonio Spada</strong><br />
                     London South Bank University<br />(United Kingdom)<br />
                    </div>
                </div>
                <div class="w3-col s6 m4">
                    <div class="w3-col s4">
                        <img width="60%"  src="images/Rumpf_Hans-Juergen_sw.jpg"   class="">
                    </div>
                    <div class="w3-four-third" >
                        <strong>Hans-Jürgen Rumpf</strong><br />
                        Medical University of Lübeck<br/>(Germany)
                    </div>
                </div>
                <div class="w3-col s6 m4">
                    <div class="w3-col s4">
                        <img  width="60%" src="images/Stark_Rudolf_sw.jpg"   class="">
                  </div>
                 <div class="w3-four-third" >
                  <strong>Rudolf Stark</strong><br />
                 Justus Liebig University Giessen<br/>(Germany)
                    </div>
                </div>
                <div class="w3-col s6 m4">
                    <div class="w3-col s4">
                        <img width="60%" src="images/Weinstein_Aviv_sw.jpg"   class="">
                      </div>
                     <div class="w3-four-third" >
                <strong>Aviv Weinstein</strong><br />
                         Ariel University<br />(Israel)<br />
                    </div>
                </div>
                <div class="w3-col s6 m4">
                    <div class="w3-col s4">
                        <img width="60%"  src="images/Young_Kimberly_sw.jpg"   class="">
                      </div>
                     <div class="w3-four-third" >
                     <strong>Kimberly Young</strong><br />
                     St. Bonaventure University<br />(United States)<br />
                    </div>
                </div>





            </div>

            <![endif]-->

            <!--IE 10-->
            <div id="ie10Div"class="w3-container">
                <div class="w3-col s6 m4">
                    <div class="w3-col s4">
                        <img width="60%" src="images/Billieux_Joel_sw.jpg">
                    </div>
                    <div class="w3-four-third">
                        <strong>Joël Billieux</strong><br />
                        University of Luxembourg<br />(Luxembourg)<br />
                    </div>
                </div>
                <div class="w3-col s6 m4">
                    <div class="w3-col s4">
                        <img  width="60%" src="images/Clark_Luke_sw.jpg"   class="">
                    </div>
                    <div class="w3-four-third">
                        <strong>Luke Clark</strong><br />
                        University of British Columbia<br/>(Canada)<br />
                    </div>
                </div>
                <div class="w3-col s6 m4">
                    <div class="w3-col s4">
                        <img  width="60%" src="images/Demetrovics_Zsolt_sw.jpg"   class="">
                    </div>
                    <div class="w3-four-third" >
                        <strong>Zsolt Demetrovics</strong><br />
                        Eötvös Loránd University<br />(Hungary)<br />
                    </div>
                </div>
                <div class="w3-col s6 m4">
                    <div class="w3-col s4">
                        <img width ="60%" src="images/Dong_Guangheng_sw.jpg"   class="">
                    </div>
                    <div class="w3-four-third" >
                        <strong>Guangheng Dong</strong><br />
                        Zhejiang Normal University Jinhua<br />(China)
                    </div>
                </div>
                <div class="w3-col s6 m4">
                    <div class="w3-col s4">
                        <img  width="60%" src="images/FernandezAranda_Fernanddo_sw.jpg"  >
                    </div>
                    <div class="w3-four-third" >
                        <strong>Fernando Fernandez-Aranda</strong><br />
                        University Hospital Bellvitge (Spain)
                    </div>
                </div>
                <div class="w3-col s6 m4">
                    <div class="w3-col s4">
                        <img width="60%" src="images/Gearhardt_Ashley_sw.jpg"  >
                    </div>
                    <div class="w3-four-third" >
                        <strong>Ashley Gearhardt</strong><br />
                        University of Michigan <br />(United States)<br />
                    </div>
                </div>
                <div class="w3-col s6 m4">
                    <div class="w3-col s4">
                        <img width="60%" src="images/King_Daniel_sw.jpg"   class="">
                    </div>
                    <div class="w3-four-third" >
                        <strong>Daniel King</strong><br />
                        University of Adelaide<br />(Australia)<br />
                    </div>
                </div>

                <div class="w3-col s6 m4">
                    <div class="w3-col s4">
                        <img width="60%" src="images/kyrios_michael_sw.jpg"   class="">
                    </div>
                    <div class="w3-four-third" >
                        <strong>Michael Kyrios</strong><br />
                        Australian National University<br />(Australia)<br />
                    </div>
                </div>
                <div class="w3-col s6 m4">
                    <div class="w3-col s4">
                        <img width="60%" src="images/Kuss_Daria_sw.jpg"   class="">
                    </div>
                    <div class="w3-four-third" >
                        <strong>Daria Kuss</strong><br />
                        Nottingham Trent University<br />(United Kingdom)
                    </div>
                </div>
                <div class="w3-col s6 m4">
                    <div class="w3-col s4">
                        <img width="60%" src="images/Rumpf_Hans-Juergen_sw.jpg"   class="">
                    </div>
                    <div class="w3-four-third" >
                        <strong>Hans-Jürgen Rumpf</strong><br />
                        Medical University of Lübeck<br/>(Germany)
                    </div>
                </div>
                <div class="w3-col s6 m4">
                    <div class="w3-col s4">
                        <img width="60%" src="images/Spada_Marcantonio_M_sw.jpg"   class="">
                    </div>
                    <div class="w3-four-third" >
                        <strong>Marcantonio Spada</strong><br />
                        London South Bank University<br />(United Kingdom)<br />
                    </div>
                </div>
                <div class="w3-col s6 m4">
                    <div class="w3-col s4">
                        <img width="60%"  src="images/Rumpf_Hans-Juergen_sw.jpg"   class="">
                    </div>
                    <div class="w3-four-third" >
                        <strong>Hans-Jürgen Rumpf</strong><br />
                        Medical University of Lübeck<br/>(Germany)
                    </div>
                </div>
                <div class="w3-col s6 m4">
                    <div class="w3-col s4">
                        <img  width="60%" src="images/Stark_Rudolf_sw.jpg"   class="">
                    </div>
                    <div class="w3-four-third" >
                        <strong>Rudolf Stark</strong><br />
                        Justus Liebig University Giessen<br/>(Germany)
                    </div>
                </div>
                <div class="w3-col s6 m4">
                    <div class="w3-col s4">
                        <img width="60%" src="images/Weinstein_Aviv_sw.jpg"   class="">
                    </div>
                    <div class="w3-four-third" >
                        <strong>Aviv Weinstein</strong><br />
                        Ariel University<br />(Israel)<br />
                    </div>
                </div>
                <div class="w3-col s6 m4">
                    <div class="w3-col s4">
                        <img width="60%"  src="images/Young_Kimberly_sw.jpg"   class="">
                    </div>
                    <div class="w3-four-third" >
                        <strong>Kimberly Young</strong><br />
                        St. Bonaventure University<br />(United States)<br />
                    </div>
                </div>
            </div>
            <!--//IE10-->

            <!--[IF !IE]> -->
     <div id="notIeDiv" class="w3-row-padding w3-center w3-section flex-container">
         <div class="w3-col m4 s6 flex-item IE_item">
             <div class="w3-third">
                 <img src="images/Billieux_Joel_sw.jpg"   class="">
             </div>
              <div class="w3-four-third ">
                 <strong>Joël Billieux</strong><br />
             University of Luxembourg<br />(Luxembourg)<br />
             </div>
         </div>
         <div class="w3-col m4 s6 flex-item IE_item" >
             <div class="w3-third" >
                 <img src="images/Clark_Luke_sw.jpg"   class="">
             </div>
              <div class="w3-four-third" >
                  <strong>Luke Clark</strong><br />
             University of British Columbia<br/>(Canada)<br />
             </div>
         </div>
         <div class="w3-col m4 s6 flex-item IE_item" >
             <div class="w3-third" >
              <img src="images/Demetrovics_Zsolt_sw.jpg"   class="">
             </div>
             <div class="w3-four-third" >
          <strong>Zsolt Demetrovics</strong><br />
             Eötvös Loránd University<br />(Hungary)<br />
             </div>
         </div>
         <div class="w3-col  m4 s6 flex-item IE_item" >
             <div class="w3-third" >
             <img src="images/Dong_Guangheng_sw.jpg"   class="">
             </div>
             <div class="w3-four-third" >
                 <strong>Guangheng Dong</strong><br />
                 Zhejiang Normal University Jinhua<br />(China)
             </div>
         </div>
          <div class="w3-col  m4 s6 flex-item IE_item" >

              <div class="w3-third" >
                 <img src="images/FernandezAranda_Fernanddo_sw.jpg"  >
             </div>
             <div class="w3-four-third" >
               <strong>Fernando Fernandez-Aranda</strong><br />
                 University Hospital Bellvitge (Spain)
              </div>
         </div>
         <div class="w3-col m4 s6 flex-item IE_item" >
             <div class="w3-third" >
             <img src="images/Gearhardt_Ashley_sw.jpg"  >
             </div>
             <div class="w3-four-third" >
               <strong>Ashley Gearhardt</strong><br />
                University of Michigan <br />(United States)<br />
             </div>
         </div>
         <div class="w3-col m4 s6 flex-item IE_item" >
             <div class="w3-third" >
                 <img src="images/King_Daniel_sw.jpg"   class="">
              </div>
             <div class="w3-four-third" >
           <strong>Daniel King</strong><br />
             University of Adelaide<br />(Australia)<br />
             </div>
         </div>
         <div class="w3-col m4 s6 flex-item IE_item" >
             <div class="w3-third" >
                 <img src="images/kyrios_michael_sw.jpg"   class="">
              </div>
             <div class="w3-four-third" >
             <strong>Michael Kyrios</strong><br />
             Australian National University<br />(Australia)<br />
             </div>
         </div>
         <div class="w3-col  m4 s6 flex-item IE_item" >
             <div class="w3-third" >
                 <img src="images/Kuss_Daria_sw.jpg"   class="">
              </div>
             <div class="w3-four-third" >
            <strong>Daria Kuss</strong><br />
                  Nottingham Trent University<br />(United Kingdom)
         </div>
         </div>
         <div class="w3-col m4 s6 flex-item IE_item" >
             <div class="w3-third" >
                 <img src="images/Rumpf_Hans-Juergen_sw.jpg"   class="">
              </div>
             <div class="w3-four-third" >
           <strong>Hans-Jürgen Rumpf</strong><br />
             Medical University of Lübeck<br/>(Germany)
             </div>
         </div>
         <div class="w3-col m4 s6 flex-item IE_item" >
             <div class="w3-third" >
                 <img src="images/Spada_Marcantonio_M_sw.jpg"   class="">
              </div>
             <div class="w3-four-third" >
             <strong>Marcantonio Spada</strong><br />
             London South Bank University<br />(United Kingdom)<br />
             </div>
         </div>
         <div class="w3-col m4 s6 flex-item IE_item" >
             <div class="w3-third" >
                 <img src="images/Stark_Rudolf_sw.jpg"   class="">
              </div>
             <div class="w3-four-third" >
              <strong>Rudolf Stark</strong><br />
             Justus Liebig University Giessen<br/>(Germany)
         </div>
         </div>
         <div class="w3-col m4 s6 flex-item IE_item" >
             <div class="w3-third" >
                 <img src="images/Weinstein_Aviv_sw.jpg"   class="">
              </div>
             <div class="w3-four-third" >
        <strong>Aviv Weinstein</strong><br />
                 Ariel University<br />(Israel)<br />
             </div>
         </div>
         <div class="w3-col  m4 s6 flex-item IE_item" >
             <div class="w3-third" >
                 <img src="images/Young_Kimberly_sw.jpg"   class="">
              </div>
             <div class="w3-four-third" >
             <strong>Kimberly Young</strong><br />
             St. Bonaventure University<br />(United States)<br />
             </div>
         </div>
      </div>
     </div>
        <!-- <![ENDIF]-->
    </div>
 </div>
<!-- Abstract -->
        <a name="abstract"></a>
        <div id="abstract">
        <div class="bgimg-7 w3-display-container">
           <div class="w3-display-middle">
                <div class="w3-hide-small w3-hide-medium w3-center w3-padding-large w3-black w3-xxlarge w3-wide w3-animate-opacity">Abstract submission</div>
                <div class="w3-hide-small w3-hide-large w3-center w3-padding-medium w3-black w3-xlarge w3-wide w3-animate-opacity">Abstract submission</div>
                <div class="w3-hide-medium w3-hide-large w3-center w3-padding-small w3-black w3-large w3-wide w3-animate-opacity">Abstract submission</div>
            </div>
        </div>
        <div class="w3-content">
            
            <p> We invite you to submit your proposals and thus contribute to the success of the 5th ICBA. It is therefore a pleasure to invite you to actively participate in the upcoming congress, which is possible through several formats:</p>

         <div class="w3-card-2 w3-container">
         <div>
            <div id="abstractType" class="w3-hide-small w3-sidebar w3-bar-block">
              <button id="button1" class="w3-bar-item w3-button active" onclick="toggleClassElementsDisplayBlock('Symposia')">Symposia</button>
              <button id="button2" class="w3-bar-item w3-button" onclick="toggleClassElementsDisplayBlock('OralPres')">Oral presentations</button>
              <button id="button3" class="w3-bar-item w3-button" onclick="toggleClassElementsDisplayBlock('MiniT')">Mini-talks</button>
            </div>
             <div id="abstractTypeB"  class=" w3-hide-medium w3-hide-large w3-bar-block">
                 <button id="button4" class="w3-bar-item w3-button active" onclick="toggleClassElementsDisplayBlock('Symposia')">Symposia <br> &nbsp;</button>
                 <button id="button5" class="w3-bar-item w3-button" onclick="toggleClassElementsDisplayBlock('OralPres')">Oral presentations <span id="crlfSpan"><br> &nbsp;</span></button>
                 <button id="button6" class="w3-bar-item w3-button" onclick="toggleClassElementsDisplayBlock('MiniT')">Mini-talks  <br> &nbsp;</button>
             </div>


             <div id="customPadding">

    <div id="Symposia" class="w3-container abstractInfo"> <!--ToDo: anpassen an small devices-->
        <h3>Symposia</h3>
        <ul>
        <li> Symposia (90 minutes) are organized by a chair person and consist of 4-6 individual oral presentations </li>
        <li>Required submission content: <br></li>
        <span class="w3-tag w3-white w3-border">title</span>
        <span class="w3-tag w3-white w3-border"> symposium content/ abstract of the symposium</span>
        <span class="w3-tag w3-white w3-border"> 3 keywords</span>
        <span class="w3-tag w3-white w3-border"> names of participants</span>
        <span class="w3-tag w3-white w3-border"> titles of individual presentations</span>
        <span class="w3-tag w3-white w3-border"> abstracts of individual presentations </span>
        <li>requirements for individual abstracts: see oral presentations</li>
        <li>Abstract max. 200 words</li>
        </ul>
  </div>

    <div id="OralPres" class="w3-container abstractInfo">
        <h3>Oral presentation</h3>
        <ul>
        <li>15 minutes</li>
        <li>Empirical study or review</li>
        <li>Required abstract content:<br>
        <span class="w3-tag w3-white w3-border"> title</span>
        <span class="w3-tag w3-white w3-border"> background</span>
        <span class="w3-tag w3-white w3-border"> method</span>
        <span class="w3-tag w3-white w3-border"> results</span> 
        <span class="w3-tag w3-white w3-border">conclusions</span>
        <span class="w3-tag w3-white w3-border"> 3 keywords</span>
        </li>
        <li>Maximum 200 words</li>
        </ul>
    </div>

      <div id="MiniT" class="w3-container abstractInfo">
        <h3>Mini-talks</h3>
        <ul>
        <li>5 minutes </li>
        <li>Required abstract content:<br>
       <span class="w3-tag w3-white w3-border"> title</span>
       <span class="w3-tag w3-white w3-border"> background</span>
       <span class="w3-tag w3-white w3-border"> method</span>
       <span class="w3-tag w3-white w3-border"> results</span>
       <span class="w3-tag w3-white w3-border"> conclusions</span>
       <span class="w3-tag w3-white w3-border"> 3 keywords</span> </li>
        <li>Maximum 200 words</li>
        </ul>
      </div>
    </div>
</div>
        </div>
        <div class="w3-container">
            <h3>Topics of discussion</h3>
            <ul class="w3-ul">
            <li>   Gambling Disorder</li>
            <li>   Internet Gaming Disorder <br> and other types of specific Internet Use Disorders, incl. Social Networking Addiction, Internet-Shopping Addiction, Cybersex Addiction</li>
            <li>   Buying Disorder</li>
            <li>   Hypersexual Behavior</li>
            <li>   Food Addiction</li>
            <li>   Exercise Addiction</li>
            <li>   Other excessive behaviors or non-substance addiction disorders</li>
            </ul>
         </div>
            <div class="w3-panel w3-container w3-pale-orange w3-leftbar w3-rightbar w3-border-orange w3-center w3-col s12">
             <h3><b>Deadlines for abstract submission</b></h3>
             <div style="margin-bottom: 10px" class="w3-large">
                    <b>Symposia:</b> 15.11.2017<br>
                     <b>Individual oral presentations and mini-talks:</b> 15.11.2017
            </div></div>
            <div class="w3-container">
               <p>
                All abstracts must be written in English. Accepted abstracts will be published in the Journal of Behavioral Addictions. Final acceptance and publication of the abstracts requires payment of conference fees. <b class="w3-text-orange">Abstract submission starts in September.</b>
                </p>
            </div>
      </div>
        <div class="w3-container">
       
        </div>
        </div>
        </div>
        </div>
<!-- Registration -->
        <a name="registration"></a>
        <div id="registration">
        <div class="bgimg-4 w3-display-container">
            <div class="w3-display-middle">
                <div class="w3-hide-small w3-hide-medium w3-center w3-padding-large w3-black w3-xxlarge w3-wide w3-animate-opacity">Registration</div>
                <div class="w3-hide-small w3-hide-large w3-center w3-padding-medium w3-black w3-xlarge w3-wide w3-animate-opacity">Registration</div>
                <div class="w3-hide-medium w3-hide-large w3-center w3-padding-small w3-black w3-large w3-wide w3-animate-opacity">Registration</div>
            </div>
        </div>

        <div class="w3-content" >
            <div class="w3-row">

                <p>
                <h3>Social Dinner</h3>
                
                <ul>
                    <li>Tuesday, April 24 at 7pm</li>
                    <li>60 Euro</li>
                    <li>Including dinner buffet and all beverages (softdrinks, beer, wine, coffee)</li>
                </ul>
                </p>
                <!--Registration NotOpen -->
              <div class="w3-container">
                <div class="w3-twothird">
                  <h3>Registration Fees</h3>
                  <p>The following fees include:</p>
                  <ul>
                      <li>Welcome Reception on the opening night</li>
                      <li>pastries and beverages in coffee breaks on all three conference days</li>
                      <li>snack buffet, water and hot drinks in lunch breaks on all three conference days</li>
                  </ul>
              </div>
                <div class="w3-margin-top w3-third">
                    <center>
                        <a href="https://www.conftool.pro/icba2018/"  title="Register">
                            <button id="regBtn" class="w3-button w3-round">
                                <b>to registration platform </b>
                            </button>
                            <br>
                            <img class="w3-hover-opacity " src="images/registration.png" style="margin:20px;width:30%"/>
                        </a>
                    </center>
                </div>
              </div>
                <div class="w3-col s0 m0 l1">&nbsp;</div>
                <table class="w3-card  w3-table w3-bordered w3-col s12 m12 l10" style="margin-bottom:20px">
                    <thead>
                    <th></th>
                    <th>Early Bird Registration</th>
                    <th>Regular  Registration</th>
                    <th>Late / Onsite Registration</th>

                    </thead>


                    <tbody>
                    <tr class="w3-text-orange">
                        <td></td>
                        <td>until 31.12.2017</td>
                        <td>01.01. - 31.03.2018</td>
                        <td>after 31.03.2018</td>
                    </tr>
                        <tr>
                            <td>Non ISSBA-members</td>
                            <td>360€</td>
                            <td>410€</td>
                            <td>460€</td>
                        </tr>
                        <tr>
                            <td>ISSBA-members</td>
                            <td>270€</td>
                            <td>320€</td>
                            <td>370€</td>
                        </tr>
                    </tbody>
                </table>
                
                
                <h3>Registration Tool</h3>
<!--                 <div class="w3-panel w3-light-grey w3-leftbar w3-rightbar w3-col s12 w3-border-grey">-->
<!--                    <p>Registration starts in September. </p>-->
<!--                </div>-->
<!--                <div class="w3-col s0 m2 l3">&nbsp;</div>-->
<!--                <div class="w3-col w3-padding-64 s12 m8 l6">-->
<!--                    <img src="images/registration.png" style="margin:20px;width:60%"/>-->
<!--                </div>-->


                <!--Registration Open-->

 <p>Registration is administrated via an external tool. There, you may create a user account to submit a contribution, enter or view reviews and access the results of the reviewing process.
                    To do so, please follow the link or click on the image below. </p>
               <div class="w3-hover-text-orange w3-center">
                  <span class="fa fa-mail-forward"></span>
                   <a href="https://www.conftool.pro/icba2018/"  title="Register"><span class="w3-large">  <b>to registration platform </b></span></a>
                <span class="fa fa-mail-reply"></span>
              </div>

               <div class="w3-container">
               <div class="w3-opacity w3-grayscale-max w3-hover-grayscale-off  w3-hover-opacity-off" style="padding: 0 8px 0 8px;">
                    <a href="https://www.conftool.pro/icba2018/" title="To Registration"><img src="images/conftool.png" width="100%"/></a>
               </div>
               </div>


            </div>           
        </div>
        </div>

        <!-- Scientific committee -->
        <a name="scientificcommittee"></a>
        <div id="scientificcommittee">
        <div class="bgimg-3 w3-display-container">
             <div class="w3-display-middle">
                <div class="w3-hide-small w3-hide-medium w3-center w3-padding-large w3-black w3-xxlarge w3-wide w3-animate-opacity">Scientific committee</div>
                <div class="w3-hide-small w3-hide-large w3-center w3-padding-medium w3-black w3-xlarge w3-wide w3-animate-opacity">Scientific committee</div>
                <div class="w3-hide-medium w3-hide-large w3-center w3-padding-small w3-black w3-large w3-wide w3-animate-opacity">Scientific committee</div>
            </div>
        </div>

        <div class="w3-content">
        <h2>ISSBA board members</h2>
        <div class="flex-container">
            <div class="flex-item IE_item"> <b>Zsolt Demetrovics (President)</b><br/> Eötvös Loránd University, (Hungary) </div>
            <div class="flex-item IE_item"> <b>Joël Billieux</b><br/> University of Luxembourg, (Luxembourg)</div>
            <div class="flex-item IE_item"> <b>Matthias Brand</b><br/> University of Duisburg-Essen, (Germany)  </div>
            <div class="flex-item IE_item"> <b>Mark Griffiths</b><br/> Nottingham Trent University, (United Kingdom)  </div>
            <div class="flex-item IE_item"> <b>Susumu Higuchi</b><br/> National Hospital Organization Kurihama Medical and Addiction Center, (Japan)  </div>
            <div class="flex-item IE_item"> <b>Susana Jiménez-Murcia</b><br/> University Hospital of Bellvitge, (Spain)  </div>
            <div class="flex-item IE_item"> <b>Astrid Müller</b><br/> Hannover Medical School, (Germany) </div>
            <div class="flex-item IE_item"> <b>Marc N. Potenza</b><br/> Yale University, (USA) </div>
            <div class="flex-item IE_item"> <b>Aviv Weinstein</b><br/> Ariel University, (Israel) </div>
        </div>
        <div class="w3-container w3-right-align">
            <img src="images/issba_logo.png" width="150px"/>
        </div>
        </div>
      </div>

        <!-- Venue and directions -->
        <a name="venue"></a>
        <div id="venue">
        <div class="bgimg-4 w3-display-container">
            <div class="w3-display-middle">
                <div class="w3-hide-small w3-hide-medium w3-center w3-padding-large w3-black w3-xxlarge w3-wide w3-animate-opacity">Venue and directions</div>
                <div class="w3-hide-small w3-hide-large w3-center w3-padding-medium w3-black w3-xlarge w3-wide w3-animate-opacity">Venue and directions</div>
                <div class="w3-hide-medium w3-hide-large w3-center w3-padding-small w3-black w3-large w3-wide w3-animate-opacity">Venue and directions</div>
            </div>
        </div>

        <div class="w3-content">
            <div class="w3-row">
                
                <div class="w3-col s12 m6">
                    <div style="position:relative;">
                    <img class="slides" src="images/hyatt3.jpg"/>

                    <img class="slides" src="images/hyatt2.jpg"/>
                    <img class="slides" src="images/hyatt4.jpg"/>
                        <button class="w3-button w3-display-left w3-text-white w3-large" style="border:1px solid lightgrey;border-radius:6em;margin-left:25px" onclick="plusDivs(-1)">&#10094;</button>
                        <button class="w3-button w3-display-right w3-text-white w3-large" style="border:1px solid lightgrey;border-radius:6em;margin-right:25px" onclick="plusDivs(+1)">&#10095;</button>
                        </div>
                </div>

                <div class="w3-col s12 m6 w3-left-align" >
                        <h3 class="w3-left-align">Venue</h3>
                        <p><b>Hyatt Regency</b><br>
                        Kennedy-Ufer 2A,<br />
                            Cologne, 50679 Germany<br />
                            <i class="fa fa-phone w3-hover-text-black" > </i>Hotel Tel: +49 221 828 1234<br />
                            <i class="fa fa-fax w3-hover-text-black" > </i>Fax: +49 221 828 1370<br />
                            <i class="fa fa-envelope w3-hover-text-black" > </i>Email: <a href="" id="crh">cologne.regency[at]hyatt.com</a><br/>
                            <i class="fa fa-home" ></i>cologne.regency.hyatt.com

                        </p>   

                </div>
            </div>

            <div class="w3-row w3-padding-32">
                 <div class="s12 m6 w3-col">
                        <h3 class="w3-left-align">Directions</h3>
                       <button onclick="toggleDiv('Dir1')" class="w3-margin w3-button w3-border w3-border-grey w3-block w3-left-align">
                           <span id="span1" class="fa fa-plus-square-o"></span> From the airport Cologne Bonn <span class="w3-hide-medium">(CGN) </span></button>
                       <div id="Dir1" class="w3-hide">
                        <ul class="w3-ul">
                       <li>15 km (9.5 miles) distance</li>
                        <li>  S-Bahn (suburban rail network): Line S13 operates every 20 minutes (in both directions) between the airport and Cologne main station. The journey takes about 13 minutes.  Price: approx. € 3</li>

                        <li>  Taxi: The drive takes about 20 minutes. Price: approx. € 35</li>
                        </ul>
                        </div>
                        <button onclick="toggleDiv('Dir2')" class="w3-button w3-margin  w3-border w3-border-grey w3-block w3-left-align"> 
                        <span id="span2" class="fa fa-plus-square-o"></span>  From the airport Düsseldorf <span class="w3-hide-medium">International (DUS)</span> </button>
                        <div id="Dir2" class="w3-hide">
                            <ul class="w3-ul">
                            <li> 59km (36.7 miles) distance </li>
                            <li>  RE (regional express): Line RE1 (direction: Aachen Hbf) and RE5 (Koblenz Hbf) operate every hour. These lines take you to the station Cologne Messe/Deutz. From there it is about 5 minutes by foot. The journey takes about 40 minutes.</li>
                            </ul>
                        </div>

                        <button onclick="toggleDiv('Dir3')" class="w3-button w3-margin  w3-border w3-border-grey w3-block w3-left-align"> 
                            <span id="span3" class="fa fa-plus-square-o "></span>  From the central railway station <span class="w3-hide-medium">(Hauptbahnhof) </span> </button>
                         <div id="Dir3" class="w3-hide">
                        <ul class="w3-ul">
                        <li>1 km (0.6 miles) distance</li>
                        <li>  S-Bahn (suburban rail network): Line S6, S11, S13 and S13 take you to the next station Cologne Messe/ Deutz. From there it is about 5 minutes by foot. Price: approx. € 5</li>

                        <li>  Walk: Walk in the direction of the Rhine and cross the Hohenzollern Bridge. The hotel is located on the right side. The walk takes about 10 minutes</li>

                        <li>  Taxi: The drive takes about 10 minutes.  Price: approx. € 10</li>
                        </ul>
                        </div>
                    </div>
                    <div class="w3-col m6 w3-hide-small">
                        <div id="mapvenue"></div>
                    </div>
            </div>
        </div>
        </div>
        <!-- Contact -->
        <a name="contact"></a>
        <div id="contact">
        <div class="bgimg-5 w3-display-container">
            <div class="w3-display-middle">
                <span class="w3-hide-small w3-hide-medium w3-center w3-padding-large w3-black w3-xxlarge w3-wide w3-animate-opacity">Contact</span>
                <span class="w3-hide-small w3-hide-large w3-center w3-padding-medium w3-black w3-xlarge w3-wide w3-animate-opacity">Contact</span>
                <span class="w3-hide-medium w3-hide-large w3-center w3-padding-small w3-black w3-large w3-wide w3-animate-opacity">Contact</span>
            </div>
        </div>

        <div id="contactSection" class="w3-content">
            <div class="w3-row w3-padding-64 w3-section">
                <div class="w3-left-align w3-hide-small w3-col m6 w3-large w3-margin-bottom">
                    <h3>Organisational inquiries</h3>
                    <i class="fa fa-envelope w3-hover-text-black" > </i> <span>Email:</span> <a class="infIcb">info[at]icba2018.com</a><br>
                </div>
                <div class="w3-center w3-hide-medium w3-hide-large w3-col m6 w3-large w3-margin-bottom">
                   <h3>Organisational inquiries</h3>
                    <i class="fa fa-envelope w3-hover-text-black" > </i> <span>Email: </span>  <a class="infIcb">info[at]icba2018.com</a><br>
                </div>                    

                
                <div class="w3-col w3-hide-large w3-hide-medium m6 w3-large w3-margin-bottom w3-center">
                    <h3>Scientific Information</h3>
                    Matthias Brand<br>
                    <i class="fa fa-envelope w3-hover-text-black" > </i> <span>Email:</span> <a class="mbu">matthias.brand[at]uni-due.de</a><br>
                    Astrid Müller<br>
                    <i class="fa fa-envelope w3-hover-text-black" > </i>  <span>Email:</span> <a class="amh">mueller.astrid[at]mh-hannover.de</a><br>
                </div>
                <div class="w3-col m6 w3-hide-small w3-large w3-margin-bottom w3-right-align">
                    <h3>Scientific Information</h3>
                    Matthias Brand<br>
                    <i class="fa fa-envelope w3-hover-text-black" > </i>  <span>Email:</span>  <a class="mbu">matthias.brand[at]uni-due.de</a><br>
                    Astrid Müller<br>
                    <i class="fa fa-envelope w3-hover-text-black" > </i>  <span>Email:</span>  <a class="amh">mueller.astrid[at]mh-hannover.de</a><br>
                </div>
            </div>
            <div id="logoFooter" class="w3-container w3-center w3-border-top w3-margin">
                <div class="w3-col s12 m4">
                    <img src="images/ude.png"/>
                </div>
                <div class="w3-col s12 m4">
                    <img src="images/issba_logo.png"/>
                </div>
                <div class="w3-col s12 m4">
                    <img src="images/MHH_Logo_grau.png"/>
                </div>
                
            </div>
        </div>
        </div>


        <script type="text/javascript" async defer>
            var mapvenue;
            var slideIndex = 1;

            function initMap() {
                mapvenue = new google.maps.Map(document.getElementById('mapvenue'),
                        {center: {lat: 50.9403282, lng: 6.9694595}, zoom: 16});
                var markervenue = new google.maps.Marker({
                    position: {lat: 50.9403282, lng: 6.9694595},
                    map: mapvenue,
                    title: 'Hyatt Regency Cologne'
                });
            }

            function w3_toggle(){
                if(document.getElementById('myNavList').style.display === "block"){
                     $('#mySidenav').find('.toggleHide').removeClass('w3-hide');
                    document.getElementById('myNavList').style.display = "none";


                }else{
                    document.getElementById('myNavList').style.display = "block";
                     $('#mySidenav').find('.toggleHide').addClass('w3-hide');
                }
            }

            function toggleDiv(id) {
                var x = document.getElementById(id);
                var y = document.getElementById("span"+id.substring(3));

                if (x.className.indexOf("w3-show") === -1) {
                    x.className += " w3-show";
                    y.className = "fa fa-minus-square-o";

                } else {
                    x.className = x.className.replace(" w3-show", "");
                    y.className = "fa fa-plus-square-o";
                }
            }

            //Picture Show for Hyatt Photos
            showDivs(slideIndex);

            function plusDivs(n) {
                showDivs(slideIndex += n);
            }

            function showDivs(n) {
                var i;
                var x = document.getElementsByClassName("slides");
                if (n > x.length) {slideIndex = 1} 
                if (n < 1) {slideIndex = x.length}
                for (i = 0; i < x.length; i++) {
                    x[i].style.display = "none"; 
                }
                x[slideIndex-1].style.display = "block"; 
            }


            //Allgemeine Hilfsmethode
            function toggleClassElementsDisplayBlock(infoName) {
                var i;
                var x = document.getElementsByClassName("abstractInfo");
                for (i = 0; i < x.length; i++) {
                    x[i].style.display = "none";
                }
                document.getElementById(infoName).style.display = "block";
            }

            $("#abstractType").find("button").click(function () {
                var x = document.getElementById("abstractType").children;
                for(var i = 0; i < x.length;i++){
                    $("#"+x[i].id).removeClass("active");
                }
                $(this).addClass("active");
            });
            $("#abstractTypeB").find("button").click(function () {
                var y = document.getElementById("abstractTypeB").children;
                for(var i = 0; i < y.length;i++){
                    $("#"+y[i].id).removeClass("active");
                }
                $(this).addClass("active");
            });


            function prepare(x,y,z,i){
                var a;
                if(y === ""){
                    a = z + "@" + x + ".com";
                }else {
                    if (i) {
                        a = z + "." + y + "@" + x + ".com";
                    } else {
                        a = z + "." + y + "@" + x + ".de";
                    }
                }
                return a;
            }

            function combine(){
                var cr = prepare("hyatt","regency","cologne",true);
                var mb = prepare("uni-due","brand","matthias",false);
                var am = prepare("mh-hannover","astrid","mueller",false);
                var ii = prepare("icba2018","","info",true);
                $("#crh").html(cr).attr("href","mailto:"+cr);
                $(".infIcb").html(ii).attr("href","mailto:"+ii);
                $(".mbu").html(mb).attr("href","mailto:"+mb);
                $(".amh").html(am).attr("href", "mailto:"+ am);
            }
        </script>

        <!-- Tool für Tracking der Besucherzahlen-->
        <script>
            (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
            })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

            ga('create', 'UA-104641796-1', 'auto');
            ga('send', 'pageview');
        </script>

        <!--GoogleMaps Einbindung-->
        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBIcC-tfi7hfEN7XSG6h1tKH6GZyhfFEL8&callback=initMap" async></script>
    </body>
</html>
