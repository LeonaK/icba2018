               
            <nav class="w3-bar w3-card-2 w3-card-grey" id="mySidenav">
             <ul class="w3-hide-large w3-hide-medium"><li class="w3-bar-item nav-item w3-large" onclick="w3_toggle()" title="Menu"><a><i class="fa fa-bars"></i> <span class="toggleHide"> menu</span></a></li>
             </ul>
            <ul id="myNavList" style="width:80%">
            <li id="homeLi" class="w3-bar-item nav-item"><a href="#home"> <span class="fa fa-home w3-hide-small w3-hide-large"></span> <span class="w3-hide-medium">Start</span></a></li>

            <li id="aboutLi" class=" w3-bar-item nav-item ">
                <a href="#about"> About</a>
            </li>
            <li id="keynotespeakersLi" class="w3-bar-item nav-item ">
                <a href="#keynotespeakers"><span class="w3-hide-medium">Keynote</span> speakers</a>
            </li>
            <li id="abstractLi" class="w3-bar-item nav-item ">
                <a href="#abstract">Abstract
                <span class="w3-hide-medium">submission</span>
                </a>
            </li>
            <li id="registrationLi" class="w3-bar-item nav-item ">
                <a href="#registration">Registration
                </a>
            </li>
            <li id="scientificcommitteeLi" class="w3-bar-item ">
                <a href="#scientificcommittee">Scientific committee</a>
            </li>
            <li id="venueLi" class="w3-bar-item nav-item ">
                <a href="#venue">Venue
                <span class="w3-hide-medium"> and directions</span>
                </a>
            </li>
            <li id="contactLi" class="w3-bar-item nav-item">
                <a href="#contact">Contact</a>
            </li>
            </ul>
            <div>
                <div class="w3-center nowOrange" id="dateDiv" onmouseover="this.style.cursor='pointer'">
                  <h3 class="w3-hide-small">23 - 25 April, 2018</h3>
                    <p class="w3-hide-large w3-hide-medium" style="line-height:1.2em;">23 - 25.04, <br>2018</p>
                </div>
            </div>
             <h5 class="w3-hide-large w3-hide-medium" style="color:white">
                5th International Conference on Behavioral Addictions
            </h5>
            </nav> 
            <script type="text/javascript">
                //Change Main Color on Date-Click
               $('#dateDiv').click(function(){
                    if($(this).hasClass('nowOrange')){
                        $('.colorScheme').attr('href','css/colorTheme_1.css');
                        $(this).removeClass('nowOrange');
                        $(this).addClass('nowTurq');
                    }else{
                        $('.colorScheme').attr('href','css/colorTheme_2.css');
                         $(this).removeClass('nowTurq');
                        $(this).addClass('nowOrange');
                    }
               });

           //MenuItem Active on Scrolling - Settings
           $(document).ready(function () {
            $(document).on("scroll", onScroll);
                $('a[href^="#"]').on('click', function (e) {
                    e.preventDefault();
                    $(document).off("scroll");

                    $('a').each(function () {
                        $(this).removeClass('active');
                    });
                    $(this).addClass('active');

                    var target = this.hash,
                    $target = $(target);
                    $('html, body').stop().animate({
                        'scrollTop': $target.offset().top+2
                    }, 500, 'swing', function () {
                        window.location.hash = target;
                        $(document).on("scroll", onScroll);
                    });
                });
            });

                function onScroll(event){
                    var scrollPos = $(document).scrollTop();
                    $('#myNavList').find('a').each(function () {
                        var currLink = $(this);
                        var refElement = $(currLink.attr("href"));

                        if ((refElement.position().top-60) <= scrollPos && refElement.position().top + refElement.height() > scrollPos) {
                           $('#myNavList').find('li').removeClass("active");
                            $(this).parent().addClass("active");
                        }
                        else{
                            $(this).parent().removeClass("active");
                        }
                    });
                }

            //Expand and collapse menu onclick, on small devices
                function w3_toggle(){
                    if(document.getElementById('myNavList').style.display === "block"){
                        $('#mySidenav').find('.toggleHide').removeClass('w3-hide');
                        document.getElementById('myNavList').style.display = "none";


                    }else{
                        document.getElementById('myNavList').style.display = "block";
                        $('#mySidenav').find('.toggleHide').addClass('w3-hide');
                    }
                }


                //adapt height of keynotspeakerDivs
//                $(document).ready(function () {
//
//                    $('.keynoteSpeaker .w3-row-padding').each(function() {
//                        $(this).children('.w3-col').matchHeight({
//                            byRow: byRow
//                        });
//                    });
//                });
        </script>
                    
